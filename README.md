# Tract Stack by At Risk Media | gatsby-plugin-tractstack

**lead generation websites that grow your business**
_a better way to reach buyers | optimize your messaging for each unique visitor_

Built using [Gatsby](https://gatsbyjs.com)
By [At Risk Media](https://atriskmedia.com) with love and oodles of unicorn oomph!

Version 1.2.x is the _pre-launch release_ powering [https://tractstack.com](https://tractstack.com)

## custom plugin for gatsby-starter-tractstack
- See: [gatsby-starter-tractstack](https://gitlab.com/at-risk-media/gatsby-starter-tractstack)
